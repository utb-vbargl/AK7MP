#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "aboutdialog.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    move(settings.value("position", QPoint(19, 10)).toPoint());
    resize(settings.value("size", QSize(400, 200)).toSize());

    connect(ui->priceWithoutVAT, &QLineEdit::textChanged, this, &MainWindow::updatePrice);
    connect(ui->vat10, &QRadioButton::clicked, this, &MainWindow::updatePrice);
    connect(ui->vat15, &QRadioButton::clicked, this, &MainWindow::updatePrice);
}

MainWindow::~MainWindow()
{
    settings.setValue("position", this->geometry().topLeft());
    settings.setValue("size", this->geometry().size());

    delete ui;
}

void MainWindow::updatePrice()
{
    bool success;
    double price = ui->priceWithoutVAT->text().toDouble(&success);
    if (!success) {
        ui->priceWithVAT->setText("N/A");
        return;
    }

    if (ui->vat10->isChecked()) {
        price *= 1.10;
    } else if (ui->vat15->isChecked()) {
        price *= 1.15;
    }
    ui->priceWithVAT->setText(QString("%1 %2").arg(price).arg(tr("CZK")));
}

void MainWindow::on_actionAbout_triggered()
{
    AboutDialog dlg(this);
    if (dlg.exec() == QDialog::Accepted) {
      QMessageBox::information(this, tr("About..."), tr("VAT Calculator v1.0"));
    }
}

