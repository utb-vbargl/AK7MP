<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="cs_CZ">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.ui" line="14"/>
        <source>VAT Calculator</source>
        <oldsource>VAT Calc</oldsource>
        <translation>DPH Kalkulátor</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="23"/>
        <source>Price without VAT</source>
        <translation>Cena bez DPH</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="33"/>
        <source>Price with VAT</source>
        <translation>Cena s DPH</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="67"/>
        <source>VAT</source>
        <translation>DPH</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="73"/>
        <source>10%</source>
        <translation>10%</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="83"/>
        <source>15%</source>
        <translation>15%</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="106"/>
        <source>Close</source>
        <translation>Zavřít</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="125"/>
        <source>&amp;File</source>
        <translation type="unfinished">&amp;Soubor</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="131"/>
        <source>&amp;Help</source>
        <translation type="unfinished">&amp;Pomoc</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <source>Quit</source>
        <translation>Odejít</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="144"/>
        <source>Ctrl+Q</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="149"/>
        <source>About</source>
        <translation>O Aplikaci</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="152"/>
        <source>F1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="40"/>
        <source>CZK</source>
        <translation>Kč</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="45"/>
        <source>About...</source>
        <translation>O Aplikaci</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="45"/>
        <source>VAT Calculator v1.0</source>
        <translation>VAT Kalkulátor v1.0</translation>
    </message>
</context>
</TS>
